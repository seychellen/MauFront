import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Karte} from "../common/Model";
import {map, Observable} from "rxjs";


@Component({
  selector: 'app-karte',
  templateUrl: './karte.component.html',
  styleUrls: ['./karte.component.css']
})
export class KarteComponent implements OnInit {

  @Input()
  karte$? : Observable<Karte>;
  @Input()
  karte? : Karte;

  @Input()
  klickbar?: boolean;
  @Output() kartenEvent = new EventEmitter<Karte>();

  imagePath? : string;
  imagePfad$? : Observable<string>;

  constructor() {
  }

  ngOnInit(): void {
    // das lässt sich nicht in den Constructor verschieben:
    if (this.karte) {
      // soll undefined bleiben, falls karte undefined ist:
      this.imagePath = `assets/${this.karte?.farbe}_${this.karte?.wert}.png`;
      // console.log("Karte", this.karte, "ImagePath", this.imagePath);
    }
    this.imagePfad$ = this.karte$?.pipe(map(k => `assets/${k.farbe}_${k.wert}.png`))
  }

  anklicken() {
    this.kartenEvent.emit(this.karte);
  }
}
