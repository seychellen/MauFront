import { TestBed } from '@angular/core/testing';

import { MaumauService } from './maumau.service';

describe('MaumauService', () => {
  let service: MaumauService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MaumauService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
