import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {map, Observable, share, tap} from "rxjs";
import {
  ABLAGESTAPEL_ID,
  FarbeEnum,
  Karte,
  Kartenbuendel,
  Partie,
  Spieler,
  Spielerhand, Spielzug, ZIEHSTAPEL_ID, Zustand
} from "../common/Model";
import {webSocket, WebSocketSubject} from 'rxjs/webSocket';

@Injectable({
  providedIn: 'root'
})
export class MaumauService {

  partie$: Observable<Partie | undefined>
  spieler$: Observable<Spieler | undefined>
  socket: WebSocketSubject<any>

  constructor(private http: HttpClient) {
    this.socket = webSocket('ws://localhost:8080/ws');
    // socket.subscribe(
    //   (message) => {
    //     console.log('Received message from server:', message);
    //   },
    //   (error) => {
    //     console.error('WebSocket error:', error);
    //   },
    //   () => {
    //     console.log('WebSocket connection closed.');
    //   }
    // );


    let partieId = "0c7ceb23-c692-4353-b6d5-737f7e0cde77"
    //
    // let spielerId = "3f84983f-9f07-4b6d-a48b-bf778fa7e3b2"
    let spielerId = "db19aa56-c5cd-4389-98f6-9a67f8bd75b7"

    this.partie$ = this.getPartie(partieId);
    this.spieler$ = this.partie$.pipe(map((partie: Partie | undefined) =>
      partie ? this.spielerOf(partie, spielerId) : undefined))
  }

  // getSpiel(spielId: number): Observable<Spiel> {
  //   const headers = new HttpHeaders()
  //     .set('content-type', 'application/json')
  //   // Wichtig für CORS: Nur relative URLS!
  //   return this.http.get<Spiel>(`/api/spiele/${spielId}`, {'headers': headers}).pipe(
  //     tap((resp) => console.log(`Das Spiel: `, resp)),
  //     share());
  // }
  //
  // getSpieler(spielerId: string): Observable<Spieler> {
  //   const headers = new HttpHeaders()
  //     .set('content-type', 'application/json')
  //   // Wichtig für CORS: Nur relative URLS!
  //   return this.http.get<Spieler>(`/api/spieler/${spielerId}`, {'headers': headers}).pipe(
  //     tap((resp) => console.log(`Hier spielt ${resp.name}.`)),
  //     share());
  // }
  //

  getSpielerhand(partieId: string, spielerId: string): Observable<Spielerhand> {
    const headers = new HttpHeaders()
      .set('content-type', 'application/json');
    // Wichtig für CORS: Nur relative URLS!
    return this.http.get<Spielerhand>(`/partien/${partieId}/spielerdaten/${spielerId}`, {'headers': headers});
  }


  getAblagestapel(partieId: string): Observable<Kartenbuendel> {
    const headers = new HttpHeaders()
      .set('content-type', 'application/json')
    const params = new HttpParams()
      .set('partieId', partieId);
    // Wichtig für CORS: Nur relative URLS!
    return this.http.get<Kartenbuendel>(`/ablagestapel`, {'params': params, 'headers': headers}).pipe(
      tap((resp) => console.log(`${resp.karten} Ablagestapel`)),
      share());
  }


  getZustand(partieId: string): Observable<Zustand> {
    const headers = new HttpHeaders()
      .set('content-type', 'application/json')
    const params = new HttpParams()
      .set('partieId', partieId);
    // Wichtig für CORS: Nur relative URLS!
    return this.http.get<Zustand>(`/zustaende/${partieId}`, {'params': params, 'headers': headers}).pipe(
      tap((resp) => console.log(`${resp} Ablagestapel`)),
      share());
  }


  private getPartie(partieId: string): Observable<Partie | undefined> {
    const headers = new HttpHeaders()
      .set('content-type', 'application/json')
    // Wichtig für CORS: Nur relative URLS!
    return this.http.get<Partie>(`/partien/${partieId}`, {'headers': headers}).pipe(
      tap((resp) => console.log("PartyPartyPartyParty", resp)),
      share());
  }


  ablegen(partie: Partie, spieler: Spieler, karte: Karte, wunsch?: FarbeEnum): Observable<any> {
    const headers = new HttpHeaders()
      .set('content-type', 'application/json')

    let index = this.spielerIndex(partie, spieler.id)
    let spielzug = new Spielzug(partie.id, karte.toString(), index, ABLAGESTAPEL_ID, wunsch)

    return this.http.post(`/spielzuege`, spielzug, {'headers': headers}).pipe(
      tap(() => console.log(`${spieler.name} hat folgende Karte gespielt:`, spielzug)),
      share());
  }

  ziehen(partie: Partie, spieler: Spieler): Observable<any> {
    const headers = new HttpHeaders()
      .set('content-type', 'application/json')

    let index = this.spielerIndex(partie, spieler.id)
    let spielzug = new Spielzug(partie.id, null, ZIEHSTAPEL_ID, index)
    console.log("ziehen", index, spielzug)

    return this.http.post(`/spielzuege`, spielzug, {'headers': headers}).pipe(
      tap(() => console.log(`${spieler.name} hat folgende Karte gezogen:`, spielzug)),
      share());
  }

  weiter(partie: Partie, spieler: Spieler): Observable<any> {
    const headers = new HttpHeaders()
      .set('content-type', 'application/json')

    let index = this.spielerIndex(partie, spieler.id)
    let spielzug = new Spielzug(partie.id, null, index, index)

    return this.http.post(`/spielzuege`, spielzug, {'headers': headers}).pipe(
      tap(() => console.log(`${spieler.name} hat weitergegeben:`, spielzug)),
      share());
  }

  //
  // ungestarteteSpiele() : Observable<number[]> {
  //   return this.http.get<Spiel[]>(`/api/spiele?typ=UNGESTARTET`).pipe(
  //       map(spiele => spiele.map(spiel => spiel.id)),
  //       share());
  // }
  //
  //
  // neuesSpiel() {
  //   const headers = new HttpHeaders()
  //     .set('content-type', 'application/json')
  //   console.log("Neues Spiel");
  //   return this.http.post<Spiel>(`/api/spiele`, {}, {'headers': headers}).pipe(
  //       tap((resp) => console.log(`Neues Spiel ${resp.id} erstellt.`)),
  //       share());
  // }
  //
  // teilnehmen(spielId: number, spielerName: string): Observable<Spieler> {
  //   const headers = new HttpHeaders()
  //     .set('content-type', 'application/json')
  //   return this.http.post<Spieler>(`/api/spieler/${spielId}`, spielerName, {'headers': headers}).pipe(
  //       tap((resp) =>
  //       console.log(`Spieler ${resp.name} nimmt am Spiel ${spielId} teil.`)),
  //       share());
  // }
  //
  // spielStarten(spielId: number): Observable<Spieler> {
  //   const headers = new HttpHeaders()
  //     .set('content-type', 'application/json')
  //   return this.http.post<Spieler>(`/api/spiele/${spielId}/starten`, {'headers': headers}).pipe(
  //     tap((resp) => console.log(`Spiel ${spielId} wurde getstartet.`)),
  //     share());
  // }

  // send() {
  //   var socket = new SockJS('/chat');
  //   var stompClient = Stomp.over(socket);
  //   stompClient.connect({}, function (frame) {
  //     console.log('Verbunden: ' + frame);
  //     stompClient.subscribe('/topic/messages', function (greeting) {
  //       console.log(JSON.parse(greeting.body).content);
  //     });
  //   });
  // }


  spielerIndex(partie: Partie, spielerId: string): number {
    return partie.spieler.findIndex(sp => sp.id == spielerId);
  }

  spielerOf(partie: Partie, spielerId: string): Spieler | undefined {
    return partie.spieler.find(sp => sp.id == spielerId);
  }

}
