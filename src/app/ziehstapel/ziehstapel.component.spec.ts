import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ZiehstapelComponent } from './ziehstapel.component';

describe('KarteComponent', () => {
  let component: ZiehstapelComponent;
  let fixture: ComponentFixture<ZiehstapelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ZiehstapelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ZiehstapelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
