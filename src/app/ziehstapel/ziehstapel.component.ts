import {Component, OnInit} from '@angular/core';
import {MaumauService} from "../service/maumau.service";
import {Partie, Spieler} from "../common/Model";
import {combineLatest} from "rxjs";

@Component({
  selector: 'app-ziehstapel',
  templateUrl: './ziehstapel.component.html',
  styleUrls: ['./ziehstapel.component.css']
})
export class ZiehstapelComponent implements OnInit {
  private spieler?: Spieler | null;
  private partie?: Partie | null;

  constructor(private maumauService: MaumauService) {
  }

  ngOnInit(): void {
    combineLatest([this.maumauService.partie$, this.maumauService.spieler$])
      .subscribe(([partie, spieler]) => {
        if (partie && spieler) {
          this.spieler = spieler
          this.partie = partie
        }
      })
  }

  zieheKarte() {
    if (this.spieler && this.partie) {
      this.maumauService.ziehen(this.partie, this.spieler).subscribe(); // FIXME
    } else {
      console.log("Fehler beim Laden des Ziehstapels: Spieler=", this.spieler, "Partie=", this.partie)
    }
  }
}
