import {Component, OnInit} from '@angular/core';
import {MaumauService} from "../service/maumau.service";
import {combineLatest, map, merge, mergeMap} from "rxjs";
import {Karte, Partie, Spieler, Zustand} from "../common/Model";

@Component({
  selector: 'app-zustand',
  templateUrl: './zustand.component.html',
  styleUrls: ['./zustand.component.css']
})
export class ZustandComponent implements OnInit {

  amSpiel: boolean | undefined
  spielerAmSpiel?: Spieler
  amSpielText: string | undefined
  wunschfarbeText: string | undefined
  ziehPlichtText: string | undefined
  private spieler?: Spieler | null
  private partie?: Partie | null

  constructor(private maumauService: MaumauService) {
  }

  ngOnInit(): void {
    combineLatest([this.maumauService.partie$, this.maumauService.spieler$])
      .subscribe(([partie, spieler]) => {
        if (partie && spieler) {
          this.spieler = spieler
          this.partie = partie

          let zustand$ =
            merge(
              this.maumauService.getZustand(partie.id),
              this.maumauService.socket.pipe(
                mergeMap(message => {
                  console.log('Zustand kommt:', message);
                  return this.maumauService.getZustand(partie.id);
                })));

          zustand$.subscribe(zustand => {
            this.partie = zustand.partie
            console.log("Partie-Kacke", this.partie)
            let spielerIndex = this.maumauService.spielerIndex(this.partie, spieler.id)
            let amSpielIndex = zustand.aktSpielerposition.index
            console.log("spielerIndex", spielerIndex, "zustand.aktSpielerposition", zustand.aktSpielerposition.index)
            this.amSpiel = amSpielIndex == spielerIndex
            this.spielerAmSpiel = this.partie.spieler[amSpielIndex]
            this.amSpielText = this.makeAmSpielText(this.partie, this.amSpiel, this.spielerAmSpiel)
            this.wunschfarbeText = this.makeWunschfarbeText(zustand)
            this.ziehPlichtText = this.makeZiehPlichtText(zustand, this.amSpiel)
          })

        } else {
          console.log("Fehler beim Laden des Zustands: Spieler=", this.spieler, "Partie=", this.partie)
        }
      })
  }

  private makeAmSpielText(partie: Partie, amSpiel: boolean, spielerAmSpiel: Spieler | undefined): string {
    if (partie.ende) return "Die Partie ist beendet"
    if (amSpiel) return "Du bist am Spiel."
    if (spielerAmSpiel) {
      return spielerAmSpiel.name + " ist am Spiel."
    }
    return "Fehler: Der aktuelle Spieler ist unbekannt!";
  }

  private makeWunschfarbeText(zustand: Zustand): string {
    if (zustand.wunschfarbe) {
      return `Die Farbe ${zustand.wunschfarbe} ist gewünscht.`
    }
    return ""
  }

  private makeZiehPlichtText(zustand: Zustand, amSpiel: boolean): string {
    let nochZuZiehen = zustand.ziehPflicht.zuZiehen - zustand.ziehPflicht.gezogen
    if (!amSpiel || nochZuZiehen <= 0 || zustand.ziehPflicht.zuZiehen == 1) return ""
    let result = `Du musst ${this.numberAsText(nochZuZiehen)} Karten ziehen`
    if (zustand.ziehPflicht.gezogen == 0) {
      result += " oder eine Sieben legen"
    }
    return result + "."
  }

  private numberAsText(num: Number): string {
    switch (num) {
      case 0:
        return "keine"
      case 1:
        return "eine"
      case 2:
        return "zwei"
      case 3:
        return "drei"
      case 4:
        return "vier"
      case 5:
        return "fünf"
      case 6:
        return "sechs"
      case 7:
        return "sieben"
      case 8:
        return "acht"
      case 9:
        return "neun"
      case 10:
        return "zehn"
      case 11:
        return "elf"
      case 12:
        return "zwölf"
      default:
        return `$num`
    }
  }

  weiter() {
    if (this.spieler && this.partie) {
      this.maumauService.weiter(this.partie, this.spieler).subscribe(); // FIXME
    } else {
      console.log("Fehler beim Laden des Zustands: Spieler=", this.spieler, "Partie=", this.partie)
    }
  }
}
