import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ZustandComponent } from './zustand.component';

describe('ZustandComponent', () => {
  let component: ZustandComponent;
  let fixture: ComponentFixture<ZustandComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ZustandComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ZustandComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
