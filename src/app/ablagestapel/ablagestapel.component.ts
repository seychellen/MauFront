import {Component, OnInit} from '@angular/core';
import {MaumauService} from "../service/maumau.service";
import {combineLatest, map, merge, mergeMap, Observable} from "rxjs";
import {Karte, Kartenbuendel} from "../common/Model";

@Component({
  selector: 'app-ablagestapel',
  templateUrl: './ablagestapel.component.html',
  styleUrls: ['./ablagestapel.component.css']
})
export class AblagestapelComponent implements OnInit {

  obersteKarte$?: Observable<Karte>;

  constructor(private maumauService: MaumauService) {
  }

  ngOnInit(): void {
    combineLatest([this.maumauService.partie$, this.maumauService.spieler$])
      .subscribe(([partie, spieler]) => {
        if (partie && spieler) {

          let ablagestapel$ =
            merge(
              this.maumauService.getAblagestapel(partie.id),
              this.maumauService.socket.pipe(
                mergeMap(_ => this.maumauService.getAblagestapel(partie.id))));

          this.obersteKarte$ = ablagestapel$.pipe(
            map((ablagestapel: Kartenbuendel) =>
              Karte.karteFromString(ablagestapel.karten[ablagestapel.karten.length - 1])));
        } else {
          console.log("FIXME")
        }
      })
  }

}
