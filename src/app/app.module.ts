import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { KarteComponent } from './karte/karte.component';
import { SpielerhandComponent } from './spielerhand/spielerhand.component';
import { ZiehstapelComponent } from "./ziehstapel/ziehstapel.component";
import { AblagestapelComponent } from "./ablagestapel/ablagestapel.component";
import { ZustandComponent } from './zustand/zustand.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {FarbenwahlComponent} from "./farbenwahl/farbenwahl.component";
import {MatDialogModule} from "@angular/material/dialog";


@NgModule({
  declarations: [
    AppComponent,
    KarteComponent,
    SpielerhandComponent,
    ZiehstapelComponent,
    AblagestapelComponent,
    ZustandComponent,
    FarbenwahlComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatDialogModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
