export const ABLAGESTAPEL_ID = -1;
export const ZIEHSTAPEL_ID = -2;


export class ZiehPflicht {
  constructor(zuZiehen: number, gezogen: number) {
    this.zuZiehen = zuZiehen;
    this.gezogen = gezogen;
  }
  zuZiehen: number;
  gezogen: number
}

export class Spielerposition {
  constructor(index: number) {
    this.index = index;
  }
  index: number;
}

export class Zustand {
  constructor(imUhrzeigensinn: boolean, ziehPflicht: ZiehPflicht, wunschfarbe: FarbeEnum,
              aktSpielerposition: Spielerposition, partie: Partie) {
    this.imUhrzeigensinn = imUhrzeigensinn;
    this.ziehPflicht = ziehPflicht;
    this.wunschfarbe = wunschfarbe;
    this.aktSpielerposition = aktSpielerposition;
    this.partie = partie;
  }
  imUhrzeigensinn: boolean;
  ziehPflicht: ZiehPflicht;
  wunschfarbe: FarbeEnum;
  aktSpielerposition: Spielerposition;
  partie: Partie;
}

export class Karte {
  constructor(wert: WertEnum, farbe: FarbeEnum) {
    this.wert = wert;
    this.farbe = farbe;
  }
  wert: WertEnum;
  farbe: FarbeEnum;

  static karteFromString(str: String): Karte {
    let parts = str.split('_')
    let farbe = farbeEnumFromString(parts[0])!;
    let wert = wertEnumFromString(parts[1])!;
    return new Karte(wert, farbe)
  }

  toString(): string {
    return `${this.farbe}_${this.wert}`
  }
}

export class Spielzug {
  constructor(partieId: string, karte: string | null, von: number, nach: number, wunschfarbe?: FarbeEnum) {
    this.partieId = partieId;
    this.karte = karte;
    this.von = von;
    this.nach = nach;
    this.wunschfarbe = wunschfarbe;
  }
  partieId : string;
  karte: string | null;
  von: number;
  nach: number;
  wunschfarbe: FarbeEnum | undefined;
}


export class Spieler {
  constructor(id: string, name: string) {
    this.id = id;
    this.name = name;
  }
  id : string;
  name: string;
}

export class Partie {

  constructor(id: string, start: string | undefined, ende: string | undefined, spieler: Spieler[]) {
    this.id = id;
    this.start = start;
    this.ende = ende;
    this.spieler = spieler;
  }

  id: string;
  start: string | undefined;
  ende: string  | undefined;
  spieler: Spieler[];
}

export enum WertEnum {
  Z7= '7', Z8 = '8', Z9 = '9', Z10 = '10', B = 'B', D = 'D', K = 'K', A = 'A'
}

export function wertEnumFromString(value: string): WertEnum | undefined {
  for (const enumValue in WertEnum) {
    if (WertEnum[enumValue as keyof typeof WertEnum] === value) {
      return WertEnum[enumValue as keyof typeof WertEnum];
    }
  }
  return undefined;
}

export enum FarbeEnum {
  KARO = 'KARO', HERZ = 'HERZ', PIK = 'PIK', KREUZ = 'KREUZ'
}

export function farbeEnumFromString(value: string): FarbeEnum | undefined {
  for (const enumValue in FarbeEnum) {
    if (FarbeEnum[enumValue as keyof typeof FarbeEnum] === value) {
      return FarbeEnum[enumValue as keyof typeof FarbeEnum];
    }
  }
  return undefined;
}

export class Kartenbuendel {

  constructor(karten: string[]) {
    this.karten = karten;

  }
  karten : string[];
}


export class Spielerhand {

  constructor(kartenbuendel: Kartenbuendel, ablagestapelKarte: string, spielerAmSpiel: string,
              amSpiel: boolean, wunschfarbe: FarbeEnum, zuZiehen: number,
              spielerMitLetzterKarte: string[], spielinfo: string) {
    this.kartenbuendel = kartenbuendel;
    this.ablagestapelKarte = ablagestapelKarte;
    this.spielerAmSpiel = spielerAmSpiel;
    this.amSpiel = amSpiel;
    this.wunschfarbe = wunschfarbe;
    this.zuZiehen = zuZiehen;
    this.spielerMitLetzterKarte = spielerMitLetzterKarte;
    this.spielinfo = spielinfo

  }
  kartenbuendel : Kartenbuendel;
  ablagestapelKarte: string;
  spielerAmSpiel: string;
  amSpiel: boolean;
  wunschfarbe: FarbeEnum;
  zuZiehen: number;
  spielerMitLetzterKarte: string[];
  spielinfo: string;
}
