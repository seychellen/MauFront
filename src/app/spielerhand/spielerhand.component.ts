import {Component, OnInit} from '@angular/core';
import {FarbeEnum, Karte, Partie, Spieler, WertEnum} from "../common/Model";
import {MaumauService} from "../service/maumau.service";
import {combineLatest, map, merge, mergeMap, Observable} from "rxjs";
import {FarbenwahlComponent} from "../farbenwahl/farbenwahl.component";
import {MatDialog} from "@angular/material/dialog";

@Component({
  selector: 'app-spielerhand',
  templateUrl: './spielerhand.component.html',
  styleUrls: ['./spielerhand.component.css']
})
export class SpielerhandComponent implements OnInit {

  karten$?: Observable<Karte[]>;
  private spieler?: Spieler;
  private partie?: Partie;

  constructor(private maumauService: MaumauService,
              private dialog: MatDialog) {

  }

  ngOnInit(): void {
    combineLatest([this.maumauService.partie$, this.maumauService.spieler$])
      .subscribe(([partie, spieler]) => {
        if (partie && spieler) {
          this.spieler = spieler
          this.partie = partie

          let spielerhand$ =
            merge(
              this.maumauService.getSpielerhand(partie.id, spieler.id),
              this.maumauService.socket
                .pipe(
                  mergeMap(_ => this.maumauService.getSpielerhand(partie.id, spieler.id)
                  )));

          this.karten$ = spielerhand$.pipe(
            map(spielerhand =>
              spielerhand.kartenbuendel.karten.map(karteStr => Karte.karteFromString(karteStr))))

        } else {
            console.log("Fehler beim Laden der Spielerhand: Spieler=", this.spieler, "Partie=", this.partie)
        }
      })
  }

  ablegen(karte: Karte) {
    if (this.spieler && this.partie) {
      if (karte.wert == WertEnum.B) {
        this.openDialog(this.spieler, this.partie, karte);
      } else {
        this.maumauService.ablegen(this.partie, this.spieler, karte).subscribe();
      }
    } else {
      console.log("Fehler beim Laden der Spielerhand: Spieler=", this.spieler, "Partie=", this.partie)
    }
  }

  private openDialog(spieler: Spieler, partie: Partie, karte: Karte): void {
    let myData = {spieler: spieler, farbe: FarbeEnum.KARO}
    const dialogRef = this.dialog.open(FarbenwahlComponent, {
      width: '250px',
      data: myData
    });

    dialogRef.afterClosed().subscribe((_: Boolean) => {
      this.maumauService.ablegen(partie, spieler, karte, myData.farbe).subscribe();
    });
  }

}
