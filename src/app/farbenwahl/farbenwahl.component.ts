import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from "@angular/material/dialog";
import {FarbeEnum, farbeEnumFromString} from "../common/Model";

@Component({
  selector: 'app-farbenwahl',
  templateUrl: './farbenwahl.component.html',
  styleUrls: ['./farbenwahl.component.css']
})
export class FarbenwahlComponent implements OnInit {

  farbe: FarbeEnum | undefined

  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {
  }

  ngOnInit(): void {
  }

  click(farbeStr: string) {
    this.data.farbe = farbeEnumFromString(farbeStr)
  }

}
