import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FarbenwahlComponent } from './farbenwahl.component';

describe('FarbenwahlComponent', () => {
  let component: FarbenwahlComponent;
  let fixture: ComponentFixture<FarbenwahlComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FarbenwahlComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FarbenwahlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
